var errorForm;

//Проверка Email используя Regular Expressions
function checkEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

//Показ ошибок
function showError(container, errorMessage) {
    container.className = 'error';
    var msgElem = document.createElement('span');
    msgElem.className = "error-message";
    msgElem.innerHTML = errorMessage;
    container.appendChild(msgElem);
}

//Сброс ошибок
function resetError(container) {
    container.className = '';
    if (container.lastChild.className == "error-message") {
        container.removeChild(container.lastChild);
    }
}

//Проверка имени
function validateName(name) {
    errorForm = true;
    resetError(name.parentNode);
    if (!name.value) {
        showError(name.parentNode, ' Укажите имя');
        errorForm = false;
    }
    return errorForm;
}

//Проверка Email
function validateEmail(email) {
    errorForm = true;
    resetError(email.parentNode);
    if (!email.value) {
        return errorForm;
    } else {
        if (!checkEmail(email.value)) {
            showError(email.parentNode, ' Укажите правильный email');
            errorForm = false;
        }
    }
    return errorForm;
}

//Проверка сообщения
function validateMsg(msg) {
    errorForm = true;
    resetError(msg.parentNode);
    if (!msg.value) {
        showError(msg.parentNode, ' Сообщение не может быть пустым');
        errorForm = false;
    }
    return errorForm;
}

