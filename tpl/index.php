<?php require_once("header.php")
// Вывод ошибок?>
<?php if ((isset($errorMsg) && $errorMsg) || isset($_SESSION['errorMsg'])) { ?>
    <div class="row">
    <div class="post">
        <label class="alert alert-danger"><?php echo "Отзыв не отправлен: <br>" . isset($_SESSION['errorMsg']) ? $_SESSION['errorMsg'] : $errorMsg ?></label>
        <hr/>
    </div>
<?php }
// Вывод сообщений
foreach ($arr as $item) { ?>
    <div class="post col-md-6 col-md-offset-3">
        <label class="post-title"><?php echo $item['id'] . ". " . $item['name'] . " (" . $item['data'] . ") ";
            if (!empty($item['email'])) {?>
            <a href="mailto:<?php echo $item['email'] ?>">Email</a>
            <?php }?>
        </label>
        <?php if (!empty($item['imgsrc']) && file_exists($item['imgsrc'])) {
            echo "<img src=" . $item['imgsrc'] . ">";
        } ?>
        <p class="post-meta">
            <?php echo $item['message']; ?>
        </p>
    </div>
<?php } ?>
        </div>
<div class="row">
    <div class="col-md-3 col-md-offset-6">
        <table class="table">
            <tbody>
            <tr>
                <td style="border: 0;">
                    <ul class="pagination pull-right">
                        <?php if ($page > 1) { // Пагинация
                            $prev = $page - 1;
                            echo "<li><a href='?page=1' class='ng-binding'><<</a></li>";
                            echo "<li><a href='?page=$prev' class='ng-binding'><</a></li>";
                        } else {
                            if ($pages !== 0) {
                                echo "<li class='ng-scope disabled'><a href='?page=' class='ng-binding'><<</a></li>";
                                echo "<li class='ng-scope disabled'><a href='?page=' class='ng-binding'><</a></li>";
                            }
                        }
                        for ($i = $page - 2; $i < $page + 2; $i++) {
                            if ($i > 0 && $i <= $pages) {
                                if ($i == $page) {
                                    echo "<li class='active'><a href='?page=$i' class='ng-binding'>" . $i . "</a></li>";
                                } else {
                                    echo "<li><a href='?page=$i' class='ng-binding'>" . $i . "</a></li>";
                                }
                            }
                        }
                        if ($page < $pages) {
                            $next = $page + 1;
                            echo "<li><a href='?page=$next'>></a></li>";
                            echo "<li><a href='?page=$pages'>>></a></li>";
                        } else {
                            if ($pages !== 0) {
                                echo "<li class='ng-scope disabled'><a href='?page=$pages'>></a></li>";
                                echo "<li class='ng-scope disabled'><a href='?page=$pages'>>></a></li>";
                            }
                        }
                        ?>
                    </ul>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
<div class="row">
    <div class="col-md-3 col-md-offset-5">
        <form method="post" name="main" enctype="multipart/form-data">
            <table>
                <tbody>
                <tr>
                    <td>
                        Имя*
                    </td>
                    <td>
                        <input type="text" name="name" value="<?php echo isset($_SESSION['name']) ? $_SESSION['name'] : ''?>" onblur="return validateName(this.form.name)" required/>
                        <input type="hidden" id="check" name="check" value=""/>
                    </td>
                </tr>
                <tr>
                    <td>
                        Email
                    </td>
                    <td>
                        <input type="email" name="email" value="<?php echo isset($_SESSION['email']) ? $_SESSION['email'] : ''?>" onblur="return validateEmail(this.form.email)">
                    </td>
                </tr>
                </tbody>
            </table>
            Сообщение*
            <label>
                <textarea rows="10" class="form-control" name="msg" onblur="return validateMsg(this.form.msg)"
                          required><?php echo isset($_SESSION['msg']) ? $_SESSION['msg'] : ''?></textarea>
            </label><br>
                <?php // Вывод ошибки формата файла
                if (isset($_SESSION['errorFile'])) { ?><label class="alert alert-danger">
                    <?php echo $_SESSION['errorFile']; ?>
                    </label><br/> <?php } ?>
                    Файл для загрузки: <input name="picture" type="file"/> <br/>
            <input type="submit" name="mainform" class="btn btn-primary form-control"
                   onclick="this.form.check.value = 'stopSpam'"
                   value="Оставить отзыв"/>
        </form>
        <hr/>
    </div>
</div>
<?php require_once("footer.php") ?>
