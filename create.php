<?php
$config = require_once("config.php");
$pageTitle = "Create New Blog Post";
$blogTitle = $config['blogTitle'] . " | " . $pageTitle;
$fileJSON = $config['filejson'];
require_once("FileDB.php");

if(isset($_SESSION["auth"])) {
    require_once("tpl/create.php");
    if(isset($_POST["summary"]) && isset($_POST["title"]) && isset($_POST["body"]) && !empty($_POST["summary"]) && !empty($_POST["title"]) && !empty($_POST["body"])) {
        $wtf = new FileDB($fileJSON);
        $wtf->createPost($_POST["title"], $_POST["summary"], $_POST["body"]);
        header("Location: index.php");
    } else {

    }
} else {
    header("Location: index.php");
}