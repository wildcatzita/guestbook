<?php

class MySQLDatabase extends mysqli {
    public function __construct($host, $user, $pass, $db) {
        parent::__construct($host, $user, $pass, $db);

        if (mysqli_connect_error()) {
            die('Ошибка подключения (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }
    }

    public function getAllMsg($sql) { // Получаем кол-во записей в БД
        return $this->query($sql)->num_rows;
    }

    public function readData($sql) { // Считываем данные
        $result = $this->query($sql);
        if (!$result) {
            die($this->error);
        } else {
            $arr = [];
            while ($row = $result->fetch_assoc()) {
                $arr[] = $row;
            }
            $result->free();
            return $arr;
        }
    }

    public function insertData($name, $email, $msg, $imgsrc) { // Вставляем данные в БД
        $now = date("Y-m-d H:i:s");
        return $this->query("INSERT into messages (name, email, message, imgsrc, data) VALUES ('$name', '$email', '$msg', '$imgsrc', '$now')");
    }
}
