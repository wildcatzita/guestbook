<?php session_start(3600);
require_once(__DIR__ . '/Db.php');
require_once(__DIR__ .'/FileDB.php');

$mysqli = new MySQLDatabase(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

//Проверка полей с формы
if (isset($_POST['name']) && isset($_POST['msg'])) {
    $imgsrc = '';
    $email = '';
    if ($_POST['check'] == 'stopSpam') {
        unset($_POST['check']);
        if (mb_strlen($_POST['name']) > 1) { // Имя валидно
            $name = $mysqli->real_escape_string($_POST['name']);
            if (mb_strlen($_POST['msg']) > 1) { // Сообщение валидно
                $msg = $mysqli->real_escape_string($_POST['msg']);
                if (isset($_POST['email']) && mb_strlen($_POST['email']) > 0) { // Проверка email
                    if (!preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $_POST['email'])) {
                        $errorMsg .= "Error email<br>"; // Ошибка в email
                        unset($_POST['email']);
                    } else {
                        $email = $mysqli->real_escape_string($_POST['email']);
                    }
                }
                if (count($_FILES) > 0) { // Проверка загружаемого файла
                    $typeFile = mb_substr($_FILES['picture']['type'], strripos($_FILES['picture']['type'], '/') + 1);
                    $uploads_dir = './uploads';
                    if ($typeFile) { // Будем проверять на тип файла
                        if (($typeFile === "jpeg" && $_FILES['picture']['error'] === 0) || ($typeFile === "png" && $_FILES['picture']['error'] === 0) || ($typeFile === "gif" && $_FILES['picture']['error'] === 0)) {
                            $tmp_name = $_FILES["picture"]["tmp_name"];
                            $nameFile = '_' . time() . '_' . $_FILES["picture"]["name"];
                            if (move_uploaded_file($tmp_name, "$uploads_dir/$nameFile")) {
                                $_SESSION['imgsrc'] = "$uploads_dir/$nameFile";
                                unset($_SESSION['errorFile'], $_SESSION['errorMsg']);
                            }
                        } else { //Сохраняю данные для автозаполнения формы (чтобыпользователь не вводил заново)
                            $_SESSION['errorFile'] = 'Формат файла должен быть gif, jpg, png';
                            $_SESSION['errorMsg'] = 'Ошибка при загрузке файла';
                            $_SESSION['name'] = htmlspecialchars($_POST['name']);
                            $_SESSION['email'] = htmlspecialchars($_POST['email']);
                            $_SESSION['msg'] = htmlspecialchars($_POST['msg']);
                            header("Location: index.php");
                        }
                    } else { // Если пользователь отказался загружать файл, после неудачных попыток
                        unset($_SESSION['errorFile'], $_SESSION['errorMsg']);
                    }
                }
                if (!isset($_SESSION['errorFile'])) {
                    $imgsrc = $_SESSION['imgsrc'];
                    if ($mysqli->insertData($name, $email, $msg, $imgsrc)) { // Запрос на запись данных в БД
                        unset($_SESSION['imgsrc'], $errorFile, $_SESSION['name'], $_SESSION['email'], $_SESSION['msg'], $_SESSION['errorMsg'], $_SESSION['errorFile']); // Удаление данных после записи в БД
                        header("Location: index.php");
                        exit;
                    } else {
                        $errorMsg = 'Ошибка при записи в БД';
                    }
                }
            } else {
                $errorMsg .= "Error msg<br>"; // Ошибка в сообщении
            }
        } else {
            $errorMsg .= "Error name<br>"; // Ошибка в имени
        }
    } else {
        die('Защита от спама!!! Включите JavaScript'); // Защита от спама сработала
    }
}
/*
if (isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] > 0 && ) { // Проверка на корректный ввод страницы
    $page = $_GET['page'];
} else {
    $page = 1;
}*/
// Получение данных из БД. Алгоритм пагинации
$pages = ceil($mysqli->getAllMsg("SELECT * FROM messages ORDER BY id DESC") / PAGE_SIZE);
if (isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] > 0 && $_GET['page'] <= $pages) { // Проверка на корректный ввод страницы
    $page = $_GET['page'];
} else {
    $page = 1;
}
$offset = ($page - 1) * PAGE_SIZE;
$arr = $mysqli->readData("SELECT * FROM messages ORDER BY id DESC LIMIT " . PAGE_SIZE . " OFFSET $offset");
include_once("tpl/index.php");




